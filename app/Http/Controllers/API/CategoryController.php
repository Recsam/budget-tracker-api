<?php

namespace App\Http\Controllers\API;

use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class CategoryController extends ApiController
{
    public function index()
    {
        $categories = Category::all();
        if (!$categories->count()) {
            return $this->successResponse('No categories found.', []);
        }
        return $this->successResponse('Categories retreived successfully', $categories);
    }

    public function store(Request $request)
    {
        $attributes = $request->all();
        $category = Category::create($attributes);
        if (!$category) {
            return $this->successResponse('Category not created.', null, false);
        }
        return $this->successResponse('Category created successfully.', $category);
    }

    public function show($id)
    {
        $categoryObj = Category::find($id);
        if (!$categoryObj) {
            return $this->successResponse('Category not found.', null, false);
        }
        return $this->successResponse('Category retreived successfully.', $categoryObj);
    }

    public function update(Request $request, $id)
    {
        try {
            $attributes = $request->all();
            $categoryObj = Category::findOrFail($id);
            $categoryObj->update($attributes);
            $categoryObj->refresh();
            return $this->successResponse('Category updated successfully.', $categoryObj);
        } catch (\Throwable $th) {
            return $this->errorResponse($th->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            Category::destroy($id);
            return $this->successResponse('Category deleted successfully.');
        } catch (\Throwable $th) {
            return $this->errorResponse($th->getMessage());
        }
    }
}
