<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Account;
use Illuminate\Http\Request;

class AccountController extends ApiController
{
    public function index()
    {
        $data = Account::all();
        if (!$data->count()) {
            return $this->successResponse('No accounts found.', []);
        }
        foreach ($data as $index => $acc) {
            $transactions = $acc->transactions;
            unset($acc['transactions']);
            $income = $transactions->filter(function ($transaction) {
                return $transaction->amount > 0.0;
            })->sum('amount');
            $expense = $transactions->filter(function ($transaction) {
                return $transaction->amount <= 0.0;
            })->sum('amount');
            $data[$index]['income'] = $income;
            $data[$index]['expense'] = $expense;
            $data[$index]['remaining'] = $income + $expense;
        }
        return $this->successResponse('Accounts retreived successfully', $data);
    }

    public function store(Request $request)
    {
        return Account::create($request->input());
    }

    public function show($id)
    {
        $accountObj = Account::find($id);
        if (!$accountObj) {
            return $this->successResponse('Account not found.', null, false);
        }
        $transactions = $accountObj->transactions;
        unset($accountObj['transactions']);
        $income = $transactions->filter(function ($transaction) {
            return $transaction->amount > 0.0;
        })->sum('amount');
        $expense = $transactions->filter(function ($transaction) {
            return $transaction->amount <= 0.0;
        })->sum('amount');
        $accountObj['income'] = $income;
        $accountObj['expense'] = $expense;
        $accountObj['remaining'] = $income + $expense;
        return $this->successResponse('Account retreived successfully.', $accountObj);
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function fetchCategories($id)
    {
        $data = [];
        $accountObj = Account::find($id);
        if (!$accountObj) {
            return $this->successResponse('Account not found.', null, false);
        }
        $categoryList = $accountObj->transactions->groupBy('category_id');
        foreach($categoryList as $index => $category) {
            $total = $category->sum('amount');
            $data[] = [
                'name' => $category[0]->category->name,
                'total' => $total,
            ];
        }
        return $this->successResponse('Categories retreived successfully.', $data);
    }
}
