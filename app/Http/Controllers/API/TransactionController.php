<?php

namespace App\Http\Controllers\API;

use App\Models\Transaction;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class TransactionController extends ApiController
{
    public function index()
    {
        $transactions = Transaction::with('category')->when(request()->query('account_id'), function(Builder $query) {
            return $query->where('account_id', request()->query('account_id'));
        })->get();
        if (!$transactions->count()) {
            return $this->successResponse('No transactions found.', []);
        }
        return $this->successResponse('Transactions retreived successfully', $transactions);
    }

    public function store(Request $request)
    {
        $attributes = $request->all();
        $transaction = Transaction::create($attributes);
        if (!$transaction) {
            return $this->successResponse('Transaction not created.', null, false);
        }
        return $this->successResponse('Transaction created successfully.', $transaction);
    }

    public function show($id)
    {
        $transactionObj = Transaction::with('category')->find($id);
        if (!$transactionObj) {
            return $this->successResponse('Transaction not found.', null, false);
        }
        return $this->successResponse('Transaction retreived successfully.', $transactionObj);
    }

    public function update(Request $request, $id)
    {
        try {
            $attributes = $request->all();
            $transactionObj = Transaction::findOrFail($id);
            $transactionObj->update($attributes);
            $transactionObj->refresh();
            return $this->successResponse('Transaction updated successfully.', $transactionObj);
        } catch (\Throwable $th) {
            return $this->errorResponse($th->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            Transaction::destroy($id);
            return $this->successResponse('Transaction deleted successfully.');
        } catch (\Throwable $th) {
            return $this->errorResponse($th->getMessage());
        }
    }
}
