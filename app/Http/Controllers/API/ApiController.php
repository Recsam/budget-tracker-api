<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponseTrait;

class ApiController extends Controller
{
    use ApiResponseTrait;
}
