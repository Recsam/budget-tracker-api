<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CategoryRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CategoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CategoryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Category::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/category');
        CRUD::setEntityNameStrings('category', 'categories');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'name' => 'icon_url',
            'label' => 'Icon',
            'type' => 'text',
        ]);
        CRUD::addColumn([
            'name' => 'name',
            'type' => 'text',
        ]);
        CRUD::addColumn([
            'name' => 'creator',
            'attribute' => 'name',
            'label' => 'Created By',
            'type' => 'relationship',
        ]);
    }

    /**
     * Define what happens when the Show operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-show
     * @return void
     */
    protected function setupShowOperation()
    {
        $this->crud->setOperationSetting('setFromDb', false);

        CRUD::addColumn([
            'name' => 'id',
            'type' => 'text',
        ]);
        CRUD::addColumn([
            'name' => 'name',
            'type' => 'text',
        ]);
        CRUD::addColumn([
            'name' => 'description',
            'type' => 'text',
        ]);
        CRUD::addColumn([
            'name' => 'icon_url',
            'label' => 'Icon',
            'type' => 'text',
        ]);
        CRUD::addColumn([
            'name' => 'creator',
            'attribute' => 'name',
            'label' => 'Created By',
            'type' => 'relationship',
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(CategoryRequest::class);

        CRUD::addField([
            'name' => 'name',
            'type' => 'text',
        ]);
        CRUD::addField([
            'name' => 'description',
            'type' => 'textarea',
        ]);
        CRUD::addField([
            'name' => 'icon_url',
            'type' => 'url',
        ]);
        CRUD::addField([
            'name' => 'creator',
            'title' => 'Created By',
            'type' => 'relationship',
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
