<?php

namespace App\Traits;

use Illuminate\Http\Response;

trait ApiResponseTrait
{

    public function successResponse($message, $data = null, $success = true)
    {
        $response = [
            'success' => $success,
            'message' => $message,
            'data' => $data,
        ];
        return response()->json($response, Response::HTTP_OK);
    }

    public function errorResponse($message, $errorResult = [], $code = Response::HTTP_NOT_FOUND)
    {
        if (request()->expectsJson()) {
            $error = [
                'code' => $code,
                'message' => $message,
            ];

            if (!empty($errorResult) && !is_string($errorResult)) {
                $errorMessages = is_array($errorResult) ? array_values($errorResult) : array_values($errorResult->toArray());
                $errorMessage = $errorMessages[0];
                $message = is_array($errorMessage) ? $errorMessage[0] : $errorMessage;
                $error['message'] = $message;
                $error['detail'] = $errorResult;
            }

            $response['error'] = $error;

            return response()->json($response, $code);
        } else {
            return $errorResult;
        }
    }
}
